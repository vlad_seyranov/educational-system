from rest_framework.response import Response
from rest_framework import generics
from . models import Posts
from . serializers import PostSerializer
from rest_framework.permissions import IsAuthenticated
from .models import UserProfile
from .serializers import UserProfileSerializer
from rest_framework.views import APIView


class PostListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Posts.objects.all().order_by('-id')
    serializer_class = PostSerializer

class PostDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Posts.objects.all()
    serializer_class = PostSerializer


class UserProfileView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        user_profile = UserProfile.objects.get(user=request.user)
        serializer = UserProfileSerializer(user_profile)
        return Response(serializer.data)