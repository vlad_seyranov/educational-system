from django.db import models
from django.contrib.auth.models import User


class Posts (models.Model):

    title = models.CharField(max_length=250)
    content = models.TextField()

    def __str__(self):
        return self.title

    @classmethod
    def create_post(cls, title, content):
        post = cls(title=title, content=content)
        post.save()
        return post

    def update_post(self, title, content):
        self.title = title
        self.content = content
        self.save()

    def delete_post(self):
        self.delete()


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.EmailField()
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    group = models.CharField(max_length=50)

    def __str__(self):
        return self.user.username