from django.contrib import admin
from . models import Posts
from .models import UserProfile


admin.site.register(Posts)
admin.site.register(UserProfile)