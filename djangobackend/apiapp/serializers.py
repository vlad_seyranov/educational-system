from rest_framework import serializers
from .models import Posts
from .models import UserProfile


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Posts
        fields = '__all__'


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ['user', 'email', 'first_name', 'last_name', 'group']