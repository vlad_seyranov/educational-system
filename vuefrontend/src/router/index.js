import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/home/Home.vue'
import store from '../store'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/news',
    name: 'news',
    component: () => import('../views/news/News.vue')
  },
  {
    path: '/news/:id',
    name: 'news-detail',
    component: () => import('../views/news/NewsDetail.vue'),
    props: true
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/auth/Login.vue')
  },
  {
    path: '/sign-up',
    name: 'sign-up',
    component: () => import('../views/auth/SignUp.vue')
  },
  {
    path: '/students',
    name: 'students',
    component: () => import('../views/Students.vue')
  },
  {
    path: '/students/:id',
    name: 'student_detail',
    component: () => import('../views/StudentDetail.vue'),
    props: true
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import('../views/UserProfile.vue')
  },
  {
    path: '/support',
    name: 'support',
    component: () => import('../views/Support.vue')
  },
  {
    path: '/menu',
    name: 'menu',
    component: () => import('../views/menu/Menu.vue')
  },
  {
    path: '/stuff',
    name: 'stuff',
    component: () => import('../views/Stuff.vue')
  },
  {
    path: '/album',
    name: 'album',
    component: () => import('../views/Album.vue')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/About.vue')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  const allowedPages = ['home', 'login', 'sign-up']
  if(!allowedPages.includes(to.name) && !store.getters.isLoggedIn) next({name: 'login'})
  else next()
})

export default router
