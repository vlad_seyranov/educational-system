export function createAuthHeaderConfig(rootState) {
  const token = rootState.auth.access;
  if (!token) {
    throw new Error("No access token found");
  }

  return {
    headers: {
      Authorization: `Bearer ${token}`
    }
  };
}