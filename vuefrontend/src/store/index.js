import { createStore } from 'vuex'
import auth from "@/store/modules/auth";
import register from "@/store/modules/register";
import posts from "@/store/modules/posts";
import students from "@/store/modules/students";
import profile from "@/store/modules/profile";
import stuff from "@/store/modules/stuff";

export default createStore({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth,
    register,
    posts,
    students,
    profile,
    stuff
  }
})
