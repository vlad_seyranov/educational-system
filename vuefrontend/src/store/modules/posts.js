import axios from "axios";
import {createAuthHeaderConfig} from "@/store/api";

export default {
  state: {
    posts: [],
    selectedPost: null
  },
  getters: {
    getPosts: state => state.posts,
    getSelectedPost: state => state.selectedPost,
    SearchPosts: state => searchQuery => {
      if (searchQuery) {
        const query = searchQuery.toLowerCase();
        return state.posts.filter(post => {
          return (
            post.title.toLowerCase().includes(query) ||
            post.content.toLowerCase().includes(query)
          );
        });
      }
      return state.posts;
    }
  },
  mutations: {
    setPosts(state, posts) {
      state.posts = posts;
    },
    setSelectedPost(state, post) {
      state.selectedPost = post;
    },
    addPost(state, post) {
      state.posts.unshift(post);
    },
    deleteNews(state, postId) {
      state.posts = state.posts.filter(post => post.id !== postId)
    },
    updateNews(state, { postId, editedTitle, editedContent }) {
      const post = state.posts.find((post) => post.id === postId);
      if (post) {
        post.title = editedTitle;
        post.content = editedContent;
      }
    },
  },
  actions: {
    async fetchNews({ commit, rootState, dispatch }) {
      try {
        const config = createAuthHeaderConfig(rootState);
        const res = await axios.get('http://127.0.0.1:8000/posts/', config);
        commit('setPosts', res.data);
      } catch (e) {
        console.error(e);
        dispatch('logout')
      }
    },
    async createNews({ commit, rootState, dispatch }, postData) {
      try {
        const config = createAuthHeaderConfig(rootState);
        const res = await axios.post('http://127.0.0.1:8000/posts/', postData, config);
        commit('addPost', res.data);
      } catch (e) {
        console.error(e);
        dispatch('logout');
      }
    },
    async deleteNews({ commit, rootState, dispatch }, postId) {
      try {
        const config = createAuthHeaderConfig(rootState);
        const res = await axios.delete(`http://127.0.0.1:8000/posts/${postId}/`, config);
        commit('deleteNews', postId)
      } catch (e) {
        console.log(e)
        }
    },
    async updateNews({ commit, rootState }, { postId, editedTitle, editedContent }) {
      try {
        const config = createAuthHeaderConfig(rootState);
        const res = await axios.put(`http://127.0.0.1:8000/posts/${postId}/`, {
          title: editedTitle,
          content: editedContent,
        }, config)
        commit('updateNews', { postId, editedTitle, editedContent })
        return true
      } catch (error) {
        console.error('Error from component.vue');
        return false
      }
    },
    async fetchNewsById({ commit, rootState, dispatch }, postId) {
      try {
        const config = createAuthHeaderConfig(rootState);
        const res = await axios.get(`http://127.0.0.1:8000/posts/${postId}/`, config);
        const post = res.data;
        commit('setSelectedPost', post);
      } catch (e) {
        console.error(e);
        dispatch('logout')
      }
    }
  }
};
