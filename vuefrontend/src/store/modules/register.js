import axios from "axios";
import router from "@/router";

export default {
  state: {
    registrationStatus: null,
  },
  getters: {
    isRegistered: state => state.registrationStatus === 'success',
  },
  mutations: {
    setRegistrationStatus(state, status) {
      state.registrationStatus = status;
    },
  },
  actions: {
    async register({ commit }, formData) {
      try {
        const response = await axios.post('http://127.0.0.1:8000/api/register/', formData);
        commit('setRegistrationStatus', 'success');
        await router.push('/login')
      } catch (error) {
        console.log(formData)
        console.error(`Ошибка при регистрации: ${error}`);
        commit('setRegistrationStatus', 'error');
      }
    },
  },
};