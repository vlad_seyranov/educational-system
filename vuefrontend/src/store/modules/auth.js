import axios from "axios";
import router from "@/router";


export default {
  state: {
    access: null,
    refresh: null,
  },
  getters: {
    isLoggedIn: state => !!state.access,
  },
  mutations: {
    initializeStore(state) {
      state.access = localStorage.getItem('access') || null;
      state.refresh = localStorage.getItem('refresh') || null;
    },
    setAccess(state, access) {
      state.access = access;
      localStorage.setItem('access', access);
    },
    setRefresh(state, refresh) {
      state.refresh = refresh;
      localStorage.setItem('refresh', refresh);
    }
  },
  actions: {
    async login({ commit, dispatch }, formData) {
      try {
        const response = await axios.post('http://127.0.0.1:8000/api/token/', formData);
        commit('setAccess', response.data.access);
        commit('setRefresh', response.data.refresh);

      } catch (error) {
        console.error(`Ошибка при входе: ${error}`);
      }
    },
    async refreshAccessToken({ commit, state }) {
      if (!state.refresh) {
        return Promise.reject(new Error("Отсутствует refresh token"));
      }

      try {
        const response = await axios.post('http://127.0.0.1:8000/api/token/refresh/', {
          refresh: state.refresh,
        });

        const access = response.data.access;
        commit('setAccess', access);
      } catch (error) {
        console.error(`Ошибка с обновлением токена ${error}`);
        commit('setAccess', null);
        commit('setRefresh', null);
        localStorage.removeItem('access');
        localStorage.removeItem('refresh');
        await router.push('/login');
        return Promise.reject(error);
      }
    },

     async logout({ commit }) {
      commit('setAccess', null);
      commit('setRefresh', null);
      localStorage.removeItem('access');
      localStorage.removeItem('refresh');
      // location.reload();
      await router.push({name: 'login' })
    },
  },
};
