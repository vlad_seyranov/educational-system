import axios from "axios";
import {createAuthHeaderConfig} from "@/store/api";

export default {
  state: {
    teachingStuff: [],
    selectedTeacher: null
  },
  getters: {
    getTeachingStuff: state => state.teachingStuff,
    searchTeacher: state => searchQuery => {
      if (searchQuery) {
        const query = searchQuery.toLowerCase();
        return state.teachingStuff.filter(teacher => {
          return (
            teacher.firstName.toLowerCase().includes(query) ||
            teacher.lastName.toLowerCase().includes(query) ||
            teacher.email.toLowerCase().includes(query) ||
            teacher.phone.toLowerCase().includes(query)
          );
        });
      }
      return state.teachingStuff;
    }
  },
  mutations: {
    setTeachingStuff(state, teachingStuff) {
      state.teachingStuff = teachingStuff
    },
  },
  actions: {
    async fetchTeachingStuff({commit, rootState, dispatch}) {
      try {
        const config = createAuthHeaderConfig(rootState);
        const res = await axios.get('https://dummyjson.com/users?limit=10', config)
        commit('setTeachingStuff', res.data.users)
      } catch (e) {
        console.error(e);
        dispatch('logout');
      }
    },
  }

}