import axios from "axios";
import {createAuthHeaderConfig} from "@/store/api";

export default {
  state: {
    userProfile: {}
  },
  getters: {
    getUserProfile: state => state.userProfile
  },
  mutations: {
    setUserProfile(state, userProfile) {
      state.userProfile = userProfile;
    }
  },
  actions: {
    async fetchUserProfile({ commit, rootState, dispatch }) {
      try {
        const config = createAuthHeaderConfig(rootState);
        const res = await axios.get('http://127.0.0.1:8000/profile/', config);
        commit('setUserProfile', res.data);
      } catch (e) {
        console.error(`ошибка получения данных профиля ${e}`);
        dispatch('logout');
      }
    }
  }
};
