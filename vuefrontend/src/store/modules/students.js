import axios from "axios";
import {createAuthHeaderConfig} from "@/store/api";

export default {
  state: {
    students: [],
    selectedStudent: null
  },
  getters: {
    getStudents: state => state.students,
    getSelectedStudent: state => state.selectedStudent,
    searchStudents: state => searchQuery => {
      if (searchQuery) {
        const query = searchQuery.toLowerCase();
        return state.students.filter(student => {
          return (
            student.firstName.toLowerCase().includes(query) ||
            student.lastName.toLowerCase().includes(query) ||
            student.username.toLowerCase().includes(query) ||
            student.email.toLowerCase().includes(query) ||
            student.phone.toLowerCase().includes(query)
          );
        });
      }
      return state.students;
    }
  },
  mutations: {
    setStudents(state, students) {
      state.students = students
    },
    setSelectedStudent(state, student) {
      state.selectedStudent = student
    }
  },
  actions: {
    async fetchStudents({commit, rootState, dispatch}) {
      try {
        const config = createAuthHeaderConfig(rootState);
        const res = await axios.get('https://dummyjson.com/users?limit=100', config)
        commit('setStudents', res.data.users)
      } catch (e) {
        console.error(e);
        dispatch('logout');
      }
    },
    async fetchStudentById({commit, rootState, dispatch}, studentId) {
      try {
        const config = createAuthHeaderConfig(rootState);
        const res = await axios.get(`https://dummyjson.com/users/${studentId}`, config)
        const student = res.data
        commit('setSelectedStudent', student)
      } catch (e) {
        console.error(e);
        dispatch('logout');
      }
    },
  }
}