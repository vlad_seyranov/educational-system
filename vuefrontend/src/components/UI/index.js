import CustomButton from "@/components/UI/CustomButton";
import CustomCircular from "@/components/UI/CustomCircular";
import CustomField from "@/components/UI/CustomField";
import CustomLogo from "@/components/UI/CustomLogo";
import CustomSwitch from "@/components/UI/CustomSwitch";
import CustomTextarea from "@/components/UI/CustomTextarea";
import CustomAlert from "@/components/UI/CustomAlert";

export default [
    CustomButton,
    CustomCircular,
    CustomField,
    CustomLogo,
    CustomSwitch,
    CustomTextarea,
    CustomAlert
]